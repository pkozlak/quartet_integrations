from quartet_capture.rules import Step, Rule, RuleContext
from time import sleep


class PKozStep(Step):
    def execute(self, data, rule_context: RuleContext):
        self.debug('Executing task....using data %s', data)
        message = self.get_or_create_parameter('MessagePK', 'no message defined')
        message2 = self.get_or_create_parameter('MessagePK2', 'no message defined')
        message3 = self.get_or_create_parameter('MessagePK3', 'no message defined')
        message4 = self.get_boolean_parameter('BoolParam', False)
        self.debug('This is the message %s', message)
        self.debug('This is the message2 %s', message2)
        self.info('This is bool parameter: %s', message4)
        self.info('Finished')


    @property
    def declared_parameters(self):
        return {
            'MessagePK': 'This is the message to display',
            'MessagePK2': 'Another message to display',
            'MessagePK3': 'And another message to display',
            'BoolParam': 'This is boolean parameter'
        }

    def on_failure(self):
        pass

        