from django.test import TestCase
from quartet_capture.models import Rule, Step, StepParameter
from quartet_capture.tasks import create_and_queue_task


class PKozStepTest(TestCase):
 
    def setUp(self) -> None:
        super().setUp()
        self.create_my_rule()

    def create_my_rule(self):
        rule = Rule.objects.create(
            name="PK my first rule",
            description="This is created by Unit test"
        )
        step = Step.objects.create(
            name="PKozStep",
            description="This is my unit test for PKozStep",
            step_class="quartet_integrations.generic.pkozlakstep.PKozStep",
            order=1,
            rule=rule
        )
        StepParameter.objects.create(
            name="Message",
            value="This is message for unit tests",
            step=step
        )



        


